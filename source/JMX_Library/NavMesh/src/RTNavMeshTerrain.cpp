#include "RTNavMeshTerrain.h"

int CRTNavMeshTerrain::FindHeight(const D3DVECTOR& vPos) const
{
	return reinterpret_cast<int (__thiscall*)(const CRTNavMeshTerrain*,const D3DVECTOR*)>(0x00cba1e0)(this, &vPos);
}

short CRTNavMeshTerrain::GetTileFlag(D3DVECTOR& vPos)
{
	return reinterpret_cast<short (__thiscall*)(CRTNavMeshTerrain*,D3DVECTOR*)>(0x00cbbff0)(this, &vPos);
}