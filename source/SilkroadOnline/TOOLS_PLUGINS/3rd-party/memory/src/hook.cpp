#include <Windows.h>
#include <stdio.h>

void placeHook(int trampoline_location, int target_location) {
    unsigned char jmp_inst[] = {0xE9, 0x00, 0x00, 0x00, 0x00};
    int distance;
    DWORD dwProtect = 0;

    distance = target_location - trampoline_location - 5;

    // Write jump-distance to instruction
    memcpy((jmp_inst + 1), &distance, 4);

    if (!VirtualProtect((LPVOID) trampoline_location, sizeof(jmp_inst), PAGE_EXECUTE_READWRITE, &dwProtect)) {
        perror("Failed to unprotect memory\n");
        return;
    }

    memcpy((LPVOID) trampoline_location, jmp_inst, sizeof(jmp_inst));

    DWORD otherProtect;
    if (!VirtualProtect((LPVOID) trampoline_location, sizeof(jmp_inst), dwProtect, &otherProtect)) {
        perror("Failed to restore protection on memory");
    }

}

void replaceOffset(int trampoline_location, int target_location) {

    char inst_offset[] = {0x00, 0x00, 0x00, 0x00};
    int distance;
    DWORD dwProtect = 0;

    int offset_location = trampoline_location + 1;

    distance = target_location - trampoline_location - 5;

    // Write jump-distance to instruction
    memcpy(inst_offset, &distance, 4);

    if (!VirtualProtect((LPVOID) offset_location, sizeof(inst_offset), PAGE_EXECUTE_READWRITE, &dwProtect)) {
        perror("Failed to unprotect memory\n");
        return;
    }

    memcpy((LPVOID) offset_location, inst_offset, sizeof(inst_offset));

    DWORD otherProtect;
    if (!VirtualProtect((LPVOID) offset_location, sizeof(inst_offset), dwProtect, &otherProtect)) {
        perror("Failed to restore protection on memory");
    }
}

void replaceAddr(int addr, int value) {
    DWORD dwProtect;

    if (!VirtualProtect((LPVOID) addr, sizeof(int), PAGE_EXECUTE_READWRITE, &dwProtect)) {
        perror("Failed to unprotect memory\n");
        return;
    }

    *((int *) addr) = value;

    DWORD otherProtect;
    if (!VirtualProtect((LPVOID) addr, sizeof(int), dwProtect, &otherProtect)) {
        perror("Failed to restore protection on memory");
    }
}

void vftableHook(unsigned int vftable_addr, int offset, int target_func) {
    replaceAddr(vftable_addr + offset * sizeof(void *), target_func);
}

void PlaceJump(int trampoline, int target) {
    unsigned char jmpInst[] = {0xEB};
    int distance;
    DWORD dwProtect = 0;

    distance = target - trampoline - 5;

    // Write jump-distance to instruction
    memcpy((jmpInst + 1), &distance, 4);

    if (!VirtualProtect((LPVOID) trampoline, sizeof(jmpInst), PAGE_EXECUTE_READWRITE, &dwProtect)) {
        perror("Failed to unprotect memory\n");
        return;
    }

    memcpy((LPVOID) trampoline, jmpInst, sizeof(jmpInst));

    DWORD otherProtect;
    if (!VirtualProtect((LPVOID) trampoline, sizeof(jmpInst), dwProtect, &otherProtect)) {
        perror("Failed to restore protection on memory");
    }
}

void PlaceJE(int trampoline, int target) {
    unsigned char jmpInst[] = {0x74};
    int distance;
    DWORD dwProtect = 0;

    distance = target - trampoline - 5;

    // Write jump-distance to instruction
    memcpy((jmpInst + 1), &distance, 4);

    if (!VirtualProtect((LPVOID) trampoline, sizeof(jmpInst), PAGE_EXECUTE_READWRITE, &dwProtect)) {
        perror("Failed to unprotect memory\n");
        return;
    }

    memcpy((LPVOID) trampoline, jmpInst, sizeof(jmpInst));

    DWORD otherProtect;
    if (!VirtualProtect((LPVOID) trampoline, sizeof(jmpInst), dwProtect, &otherProtect)) {
        perror("Failed to restore protection on memory");
    }
}

void fillNOPS(int addr, int count) {
    DWORD oldProtect;

    if (!VirtualProtect((LPVOID) addr, count, PAGE_EXECUTE_READWRITE, &oldProtect)) {
        perror("Failed to unprotect memory\n");
        return;
    }

    memset((LPVOID) addr, 0x90, count);

    if (!VirtualProtect((LPVOID) addr, count, oldProtect, &oldProtect)) {
        perror("Failed to restore protection on memory");
    }
}