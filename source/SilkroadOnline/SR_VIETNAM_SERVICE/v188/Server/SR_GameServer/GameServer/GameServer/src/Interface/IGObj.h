//
// Created by Kurama on 12/11/2022.
//
#pragma once

#include "Data.h"
#include "Debug.h"

#define ASSERT_INTERFACE() FW_ASSERT(DEBUG_LOG_MSG_TYPE_FATAL, "## Illegal ##  %s Entered! TypeID: %u", __FUNCTION__, this->GetTypeID());
#define ASSERT_INTERFACE2(x) FW_ASSERT(DEBUG_LOG_MSG_TYPE_FATAL, "## Illegal ##  %s(%d) Entered! TypeID: %u", __FUNCTION__, x, this->GetTypeID());

class IGObj {
public:
    virtual ~IGObj() = 0;

public:
    virtual int GetRefObjID() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual int GetJID() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual int GetGameID() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual WORD GetTypeID() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual SWorldID &GetWorldID(SWorldID &dwResult) const {
        ASSERT_INTERFACE()
        dwResult = {1, 1};
        return dwResult;
    }

    virtual bool IsChar() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsPC() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsNPC() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsNPCNPC() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsMonster() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsCOS() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsCart() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsVehicle() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsPet() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsMercenary() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsAttackableCOS() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsStruct() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsItem() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsExpendables() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsContainerItem() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsContainerSummon() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsSpecialty() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsPotion() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsScroll() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsBullet() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsArrow() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsBolt() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsHPPotion() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsMPPotion() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsElixir() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsGold() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsTownPortal() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsEquip() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsWeapon() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsShield() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsAccessory() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsTeleportGate() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsRobe() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsArmor() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsBow() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsCrossBow() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsSummonScroll() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsReinforceRecipe() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsReinforceProbUP() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsFreeBattleEquip() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsQuest_n_EventItem() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool IsCashItem() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool Func_48() const {
        return false;
    }

    virtual bool CanTrade() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool CanSell() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool CanBorrow() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool CanDrop() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool CanPick() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool CanRepair() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool CanRevive() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool CanUse() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual bool CanThrow() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual const char *GetCodeName() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual const char *GetCharName() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual const char *GetNickName() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual void GetPosInfo(SPosInfo &pInfo) const {
        ASSERT_INTERFACE()
    }

    virtual LIFESTATE GetLifeState() const {
        ASSERT_INTERFACE()
        return LIFESTATE_EMBRYO;
    }

    virtual MOTIONSTATE GetMotionState() const {
        ASSERT_INTERFACE()
        return MOTIONSTATE_STAND;
    }

    virtual BODYMODE GetBodyMode() const {
        ASSERT_INTERFACE()
        return BODYMODE_NORMAL;
    }

    virtual double GetParam(GOBJ_PARAM wParamID) const {
        ASSERT_INTERFACE2(wParamID)
        return 0.0f;
    }

    virtual int GetHealth() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual int GetMana() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual int GetMaxHealth() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual int GetMaxMana() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual bool IsTargeting() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual JOBSTATE GetJobState() const {
        ASSERT_INTERFACE()
        return JOBSTATE_NONE;
    }

    virtual TELEPORTSTATE GetTeleportState() const {
        ASSERT_INTERFACE()
        return TELEPORTSTATE_NONE;
    }

    virtual BYTE GetLevel() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual BYTE GetMaxLevel() const {
        ASSERT_INTERFACE()
        return 0;
    }

    virtual MONSTERCLASS GetMonsterClass() const {
        ASSERT_INTERFACE()
        return MONSTERCLASS_NONE;
    }

    virtual bool GetPartyInfo(struct SPartyInfo *) const {
        ASSERT_INTERFACE()
        return false;
    }

    /*
    virtual bool AttachCustomTeleportCapability() const {
        ASSERT_INTERFACE()
        return false;
    }

    virtual void RegisterEventHandler() {
        ASSERT_INTERFACE()
    }

    virtual void UnregisterEventHandler() {
        ASSERT_INTERFACE()
    }
*/

// TODO : STILL MISSING SOME VFT FUNCS!!!



private:
};