//
// Created by Kurama on 12/11/2022.
//
#pragma once

#include <sql.h>
#include "Base.h"

#include "Interface/IGObj.h"

#include "InstanceObj.h"

class CGObj : public IGObj, public CBase {
public:
public:
private:
    // TODO : uknow what u have todo!
    DWORD m_dwGameId; //0x0008
    char pad_000c[40]; //0x000c
    CInstanceObj *m_pObjDataInstance; //0x0034
    char pad_0038[64]; //0x0038
    SWorldID m_sObjWorldId; //0x0078
    char pad_007c[208]; //0x007c
    SQLCHAR f;
private:
BEGIN_FIXTURE()
        ENSURE_SIZE(0x14c)
        ENSURE_OFFSET(m_dwGameId, 0x0008)
        ENSURE_OFFSET(m_pObjDataInstance, 0x0034)
        ENSURE_OFFSET(m_sObjWorldId, 0x0078)
    END_FIXTURE()

    RUN_FIXTURE(CGObj)
};