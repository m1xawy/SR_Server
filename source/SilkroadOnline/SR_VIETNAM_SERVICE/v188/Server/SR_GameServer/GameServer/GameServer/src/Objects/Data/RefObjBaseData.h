//
// Created by Kurama on 12/11/2022.
//
#pragma once

class _declspec(novftable) RefObjBaseData {
public:
    virtual ~RefObjBaseData() = 0;

private:
};