//
// Created by Kurama on 12/11/2022.
//
#pragma once

#include "RefObjBaseData.h"

#include "Base.h"

#include <string>

class RefObjCommon : public RefObjBaseData {
public:
    // its like check for some stuff?
    virtual void Func_1();

private:
    char pad_0004[4]; //0x0004
    DWORD m_dwObjectId; //0x0008
    char pad_000c[4]; //0x000c
    std::string m_strObjectCode; //0x0010
    std::string m_strObjectName; //0x002c
    char pad_0048[336]; //0x0048
private:
BEGIN_FIXTURE()
        ENSURE_SIZE(0x0198) // stolen from RefObjDummy, Thanks Joymax ! ^_^
    END_FIXTURE()

    RUN_FIXTURE(RefObjCommon)
};