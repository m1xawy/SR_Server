//
// Created by Kurama on 12/11/2022.
//
#pragma once

#include "Base.h"

class CDBRecord : public CBase {
DECLARE_DYNAMIC_EXISTING(CDBRecord, 0x00add8ec)

public:
    virtual const char *GetTableName() const = 0;

    virtual CRuntimeClass const *GetRefClassRuntime() const = 0;

    virtual void Func_4();

    virtual void Func_5();

    virtual void Func_6();

    virtual void Func_7();

    virtual void Func_8();

private:
    char pad_0004[20];
private:
BEGIN_FIXTURE()
        ENSURE_SIZE(0x0018)
    END_FIXTURE()

    RUN_FIXTURE(CDBRecord)
};