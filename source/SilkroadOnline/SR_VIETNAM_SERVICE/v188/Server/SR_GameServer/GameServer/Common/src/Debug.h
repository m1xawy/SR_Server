//
// Created by Kurama on 12/11/2022.
//
#pragma once

enum DEBUG_LOG_MSG_TYPE : int {
    DEBUG_LOG_MSG_TYPE_NOTIFY = 0,
    DEBUG_LOG_MSG_TYPE_WARNING = 0x1000000,
    DEBUG_LOG_MSG_TYPE_FATAL = 0x2000001
};

// frame work
#define FW_ASSERT(type, msg, ...) reinterpret_cast<void (__cdecl *)(DEBUG_LOG_MSG_TYPE, const char *, ...)>(0x00936640)(type, msg, __VA_ARGS__)
